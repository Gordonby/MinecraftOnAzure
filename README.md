# Minecraft On Azure

This repo provides assets to run Minecraft on Azure

## Considerations

### Data

It's super important that data is backed up regularly when running your Minecraft server. The service most suited to this is the Azure Storage Account. Azure Storage Accounts have a mature set of features for the management of file backups, and can be used in a wide variety of compute platforms. They aren't particuarly well suited to super high throughput, so depending on the number of players you may find the storage a bottleneck at a certain point.

Azure Storage Accounts do have *premium* sku, however the billing model is a little different in that you pay for provisioned capacity rather than consumped. At a minimum file share size of 100GB, you may find that it won't be the cheapest option for your Minecraft storage.

### Compute

Azure file shares can be attached natively to many Azure compute services. ACI is probably the simplest option, and it does work well. However i have [experienced problems](https://github.com/itzg/docker-minecraft-bedrock-server/issues/279) when connecting to the world which has necessitated restarting the ACI multiple times.
I've found that the Azure Kubernetes Service provides a robust compute platform, and has the benefit of also providing a hosting environment for some of my other container workloads on the same infrastructure.

### Networking

UDP

## Deployment Asssets

Config | Method | Link
------ | ------ | ----
ACI | Azure CLI | 
AKS | Bicep |
